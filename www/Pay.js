/*global cordova*/
module.exports = {
  weixin: function(arg, success, failure) {
    cordova.exec(success, failure, "Pay", "weixin", [arg]);
  },
  alipay: function(arg, success, failure) {
    cordova.exec(success, failure, "Pay", "alipay", [arg]);
  }
};