package gj.cordova.plugin;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import java.util.Map;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONObject;

import org.apache.cordova.CordovaPlugin;

import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

public class Pay extends CordovaPlugin {
    private Activity activity;
    private static String WEIXIN = "weixin";
    private static String ALIPAY = "alipay";
    public static WEIXIN_PAY = 1001;
    public static IWXAPI iwxapi = null;
    private CallbackContext callbackContext = null;
    @Override
    protected void pluginInitialize() {
        Logger.d("init");
        this.activity = cordova.getActivity();
    }

    @Override
    public void onPause(boolean multitasking) {

    }
    
    @Override
    public void onResume(boolean multitasking) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        Logger.d("execute");
        JSONObject obj = args.optJSONObject(0);
        if (action.equals(WEIXIN)) {
            weixin(obj, callbackContext);
        } else if (action.equals(ALIPAY)) {
            alipay(obj, callbackContext);
        } else {
            return false;
        }
        return true;
    }

    private void weixin (JSONObject jsonObject, final CallbackContext callbackContext) {
        Logger.d("weixin");
        if (iwxapi == null) {
            String WEIXIN_KEY = preferences.getString("WEIXIN_KEY", "");
            iwxapi = WXAPIFactory.createWXAPI(this.activity, WEIXIN_KEY);
            iwxapi.registerApp(WEIXIN_KEY);
        }
        PayReq req = new PayReq();
        req.appId			= jsonObject.getString("appid");
        req.partnerId		= jsonObject.getString("partnerid");
        req.prepayId		= jsonObject.getString("prepayid");
        req.nonceStr		= jsonObject.getString("noncestr");
        req.timeStamp		= jsonObject.getString("timestamp");
        req.packageValue	= jsonObject.getString("package");
        req.sign			= jsonObject.getString("sign");
        req.extData			= "app data";
        this.callbackContext = callbackContext;
        cordova.setActivityResultCallback(this);
        iwxapi.sendReq(req);
    }

    private void alipay (JSONObject jsonObject, final CallbackContext callbackContext) {
        Logger.d("alipay");
        PayTask alipay = new PayTask(MainActivity.this);
        Map<String, String> callback = alipay.payV2(jsonObject, true);
        String resultInfo = callback.get("result");
        String resultStatus = callback.get("resultStatus");
        if (resultStatus.equals("9000")) {
            callbackContext.success("success");
        } else {
            callbackContext.error("error");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d("onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == WEIXIN_PAY) {
            if (data.getIntExtra("getType") == ConstantsAPI.COMMAND_PAY_BY_WX) {
                if (data.getIntExtra("errCode") == 0) {
                    this.callbackContext.success("success");
                } else {
                    this.callbackContext.error("error");
                }
            }
        }
    }

    private static class Logger {
        private static String TAG = "Pay";
        private static boolean DEBUG = true;
        public static void d(String str) {
            if (DEBUG) {
                Log.d(TAG, str);
            }
        }

        public static void d(String str1, String str2) {
            if (DEBUG) {
                Log.d(TAG, str1+":"+str2);
            }
        }
    }
}
