//
//  ViewController.m
//  test
//
//  Created by 高杰 on 2017/5/8.
//  Copyright © 2017年 高杰. All rights reserved.
//

#import "Pay.h"

#pragma mark - Pay
@interface Pay()

@end
@implementation Pay

static Pay *defaultPayManager = nil;
CDVInvokedUrlCommand *urlCommand = nil;

+(Pay*)defaultPayManager
{
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if(defaultPayManager == nil)
        {
            defaultPayManager = [[self alloc] init];
        }
    });
    return defaultPayManager;
}

- (void)pluginInitialize{
    NSLog(@"Pay->%@", @"init");
    defaultPayManager = self;
}

-(void) weixin:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Pay->%@", @"weixin");
    self.urlCommand = command;
    NSDictionary* options;
    if (command.arguments.count == 0) {
        options = [NSDictionary dictionary];
    } else {
        options = command.arguments[0];
    }
    NSMutableString *stamp  = options[@"timestamp"];
    PayReq* req             = [[PayReq alloc] init];
    req.partnerId           = options[@"partnerid"];
    req.prepayId            = options[@"prepayid"];
    req.nonceStr            = options[@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = options[@"package"];
    req.sign                = options[@"sign"];
    [WXApi registerApp: appId];
    [WXApi sendReq:req];
}

-(void) alipay:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Pay->%@", @"alipay");
    self.urlCommand = command;
    NSString* options;
    if (command.arguments.count == 0) {
        options = @"";
    } else {
        options = command.arguments[0];
    }
    NSString *appScheme = @"payDemo";
    [[AlipaySDK defaultService] payOrder:options fromScheme:appScheme callback:^(NSDictionary *resultDic){
    
    }];
}

+(void) paySuccess:
{
    pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:@"success"];
    [self.commandDelegate sendPluginResult: pluginResult callbackId: self.urlCommand.callbackId];
}

+(void) payError:
{
    pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:@"error"];
    [self.commandDelegate sendPluginResult: pluginResult callbackId: self.urlCommand.callbackId];
}
@end
