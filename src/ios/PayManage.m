//
//  ViewController.m
//  test
//
//  Created by 高杰 on 2017/5/8.
//  Copyright © 2017年 高杰. All rights reserved.
//

#import "PayManage.h"
#import "Pay.h"

#pragma mark - PayManage
@interface PayManage()

@end
@implementation PayManage

+(BOOL) handleOpenURL:(NSURL *) url{
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic){
            [[[Pay alloc] init] onRespA:resultDic];
        }];
        return TRUE;
    } else {
        return [WXApi handleOpenURL: url delegate: [[Pay alloc]init]];
    }
}

-(void) onRespA:(NSDictionary *)resp {
    NSString *resultStatus = [resp objectForKey:@"resultStatus"];
    CDVPluginResult *pluginResult = nil;
    if ([resultStatus isEqual:@"9000"]) {
        [[Pay defaultPayManager] paySuccess]
    } else {
        [[Pay defaultPayManager] payError]
    }
}

-(void) onResp:(BaseResp *)resp {
    if (resp.errCode == 0) {
        [[Pay defaultPayManager] paySuccess]
    } else {
        [[Pay defaultPayManager] payError]
    }
}
@end
